# 核心网络设备端口管理工具

## 如何下载
去[这里](https://gitee.com/UsirukEsikam/platinum/releases)下载最新版本

## 如何使用

解压后运行exe即可，目前有4个模块
- 查询/导出
- 端口占用
- 新增/扩装
- 拆除

### 查询/导出

用于设备端口信息的查询和导出，生成的`导出表`保存在`.\files文件夹`中
1. 点击`查询`
2. 依次选择`地区`、`数据中心`、`机楼`、`机房`和`设备`（设备列表框可多选）
3. 点击`导出`

`导出表`命名方式：`[设备名称]_[设备型号]_[机架]_[机楼]_[数据中心].xlsx`

`导出表`包含如下几个SHEET
- 设备信息：设备基础信息，其中设备ID在端口占用、和拆除模块中会用到
- 板卡信息：设备板卡信息
- 端口信息：设备端口信息，默认只导出状态为`可用`的端口，需要全部端口信息在导出时请勾选`全部端口`
- 可用统计：设备可用板卡/子卡/端口的统计信息
- 端口图：设备端口图
- 日志：设备的操作日志

`板卡`/`子卡`/`端口`编号
- 板卡：n（槽位号）
- 子卡：n/n（槽位号/子卡号）
- 端口：n/n/n（槽位号/子卡号/端口号，没有子卡时子卡号使用0）

### 端口占用

用于设备端口占用，使用`查询/导出`模块的`导出表`作为导入表
1. 导出目标设备的`导出表`
2. 填写`导出表`的`端口信息`SHEET，填写想要使用端口的`名称`、`型号`、`用途`、`项目`4列（均不可为空）
3. 浏览`导出表`所在的文件夹
4. 点击`占用`

`端口号`、`端口类型`、`状态`3列无需修改（也不要修改），文件夹中可以存在多个`导入表`，会全部读取并导入

`名称`列不要使用`-`(英文减号)，如有必须可以使用如`_`、`~`等其他字符代替

`名称`列可以使用`-n`形式的后缀，在名称前缀、型号、用途、项目都一致的情况下在生成端口图时会合并为一行

**一些例子**：端口占用    ->    端口图

```
3  交换机A          3  交换机A
4  交换机B          4  交换机B
5  交换机C    ->    5  交换机C
6  交换机D          6  交换机D
7  交换机E          7  交换机E

# 不同的设备在端口图中会单独的一行
```

```
3  交换机A          
4  交换机A          
5  交换机A    ->    3-7  交换机A
6  交换机A          
7  交换机A          

# 相同的设备会合并为一行
```

```
3  交换机-1         
4  交换机-2         
5  交换机-3   ->    3-7  交换机-1.2.3.4.5
6  交换机-4         
7  交换机-5         

# 使用`-n`后缀的相同设备也会合并为一行
```

```
3  交换机1~5         
4  交换机1~5        
5  交换机1~5  ->    3-7  交换机1~5
6  交换机1~5        
7  交换机1~5        

# 注意间隔使用的是`~`，实际上与例子2相同（相同设备会合并为一行）
```

### 新增/扩装

用于`新增`或`扩装`设备，`导入表`模版在`.\docs文件夹`中

#### 新增

在数据库中新增（或者说注册）一台新的设备，会初始化相应的板卡/子卡/端口
1. 复制`1_导入表_新增.xlsx`并填写
2. 浏览`导入表`
3. 点击`新增`

新增`导入表`注意事项
- E3-E9单元格不可为空
- 板卡信息部分（B\C\D\E列），相同的信息`务必`使用合并单元格（用于判别板卡/子卡/端口的附属关系）
- 空槽位/端口请填写`空`，只有为`空`的槽位/端口才可以进行扩装操作
- 主控、交换、监控板型号需要包含`主控板`、`交换板`、`监控板`字样
- 槽位号建议填写整数（不支持类似x~x，x-x的格式），特殊板卡（如主控板、交换板）可以按需填写
- 子卡号、数量、起始编号务必填写整数
- 多个设备可采用多个SHEET的形式同时导入

#### 扩装

为现有设备扩装板卡/子卡/光模块，只有状态为`空`的部件可以进行扩装
1. 复制`1_导入表_扩装.xlsx`并填写
2. 浏览`导入表`
3. 点击`扩装`

扩装`导入表`注意事项
- E3-E4单元格不可为空
- 选择扩装模式后，灰色的部分不需要填写
- 其他填写规则同`新增`设备，设备ID、槽位号、子卡号、端口号建议从`导出表`直接复制
- 多个设备可采用多个SHEET的形式同时导入

### 拆除

用于删除设备、板卡、子卡、光模块或者线缆
1. 选择`拆除模式`、并填写相应的`设备ID`和`部件ID`
2. 点击`拆除`

强烈建议进行操作前`导出`一份`导出表`作为备份
- 拆除设备：会删除该ID对应设备的全部信息
- 拆除板卡：板卡槽置`空`，相应的子卡、光模块等全部删除
- 拆除子卡：子卡槽置`空`，相应的光模块等全部删除
- 拆除光模块：端口置`空`，其他信息删除
- 拆除线缆：使用信息删除

## 错误处理

:x: 发问：为什么xx功能不能用了

:o: 邮件描述`操作过程`和`发生的错误`，并将`platinum.log`和相应的`导入表/输入数据`一并发送给开发者

## TODO
- 端口表导出